package com.project.orderservice.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "order_tb")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Order {
    @Id
    private int id;

    private String name;

    private int quantity;

    private Double price;



}
